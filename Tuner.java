/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author palo
 */
public class Tuner {
    float C2 = 65.4F;
    float Cs2 = 69.3F;
    float D2 = 73.4F;
    float Ds2 = 77.8F;
    float E1 = 82.4F;
    float F1 = 87.3F;
    float Fs2 = 92.5F;
    float G2 = 98.0F;
    float Gs2 = 103.8F;
    float A2 = 110F;
    float As2 = 116.5F;
    float B2 = 123.5F;
    float C3 = 130.8F;
    float Cs3 = 138.6F;
    float D3 = 146.8F;
    float Ds3 = 155.6F;
    float E3 = 164.8F;
    float F3 = 174.6F;
    float Fs3 = 185.0F;
    float G3 = 196.0F;
    float Gs3 = 207.7F;
    float A3 = 220.0F;
    float As3 = 233.1F;
    float B3 = 246.9F;
    float C4 = 261.6F;
    float Cs4 = 277.2F;
    float D4 = 293.7F;
    float Ds4 = 311.1F;
    float E4 = 329.6F;
    float F4 = 349.2F;
    float pitch;
    float noteToTune;
    float[] notes = {C2, Cs2, D2, Ds2, E1, F1, Fs2, G2, Gs2, A2, As2, B2, C3, Cs3,
    D3, Ds3, E3, F3, Fs3, G3, Gs3, A3, As3, B3, C4, Cs4, D4, Ds4, E4, F4};
    
    String[] notesStrings = {"C2", "Cs2", "D2", "Ds2", "E1", "F1", "Fs2", "G2", "Gs2", "A2", "As2", "B2", "C3", "Cs3",
    "D3", "Ds3", "E3", "F3", "Fs3", "G3", "Gs3", "A3", "As3", "B3", "C4", "Cs4", "D4", "Ds4", "E4", "F4"};

    public Tuner(float pitch) {
        this.pitch = pitch;
        autoTune();
    }
    
    /*
    finds out which note is the closest to estimated pitch and tells user
    if it is the right frequency
    */
    public void autoTune(){
        float minDiff = 1000F;
        int minDiffIndex = 0;
        for (int i = 0; i < notes.length; i++) {
            if(minDiff > Math.abs(pitch - notes[i])){
                minDiff = Math.abs(pitch - notes[i]);
                minDiffIndex = i;
            }
        }
        noteToTune = notes[minDiffIndex];
        System.out.println(notesStrings[minDiffIndex] + "\n" + pitch + "\n" + ((minDiff<0.2F) ? "OK" : "not OK"));
    }
    
    
}
