/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author palo
 */
public class Main {
    
    static final long RECORD_TIME = 1000;
    
    public static void main(String[] args) {
        while(true) {
    
            AudioRecorder recorder = new AudioRecorder();

            // creates a new thread that waits for a specified
            // time before stopping
            Thread stopper = new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(RECORD_TIME);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    recorder.finish();
                }
            });

            stopper.start();

            // start recording
            recorder.run();
        }
    }
}
